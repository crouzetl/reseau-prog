#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

 int main(int argc, char **argv) {
        int i;
        printf("PID: %d (avant)\n", getpid());
        i = fork();
        if (i != 0) {
            printf("PID: %d, résultat : %d\n",getpid(),i);
        } 
        else {
            sleep(5);
            printf("PID: %d, résultat : %d \n",getpid(),i);
        }
        printf("PID: %d (après)\n", getpid());
 }

 int main(int argc, char **argv) {
        int i, j, s, tabpid[3];
        printf("[avant fork] PID: %d\n", getpid());
        for(j = 0; j < 3; j++) {
            tabpid[j] = fork();
            if (tabpid[j] != 0 ) {
                printf("PID: %d, retour fork: %d \n",getpid(),tabpid[j]);
            } else {
                for (int i = 0 ; i < 3 ; i++) {
                    printf("%i\n", tabpid[i]);
                }
                printf("PID: %d \n",getpid());
                exit(j);
            }
        }
        for(j = 0; j < 3; j++) {
            i= waitpid(tabpid[j], &s, 0);
            if (i > 0) {
                printf("terminé PID: %d\n", i);
            }
            if (i == -1 ) {
                perror("waitpid() failed");
                exit(EXIT_FAILURE);
            }
            if ( WIFEXITED(s) ) {
                int es = WEXITSTATUS(s);
                printf("Exit status was %d\n", es);
            }
            sleep(1);
        }
    }

    int main(int argc, char **argv) {
    char *tab[argc];
    int i = 1;
    if (argc < 3){
        printf("pas assez d'arguments\n");
        exit(1);
    }

    int pid = fork();
    if (pid==0){
        while (i < argc){
            tab[i-1] = argv[i];
            i++;
        }
        tab[argc-1] = NULL;
        execvp(tab[0],tab);
    } else {
        waitpid(pid,NULL,0);
    }
    return 0;
}
int main(int argc, char** argv) {
execlp("ps" ,"ps" ,"-l" ,NULL);
}



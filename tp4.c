int main(int argc, char **argv) {
    int fd_in = open(argv[1],O_RDONLY);
    int fd_out = open(argv[2],O_WRONLY | O_TRUNC);
    int nbread;
    unsigned char *buffer = malloc(4096 * sizeof(unsigned char));  
    if (fd_in < 0){
        if (errno == ENOENT) {
    		perror(argv[1]);
    	}
    }
    if (fd_out < 0) {
    	if (errno == ENOENT) {
    		perror(argv[2]);
    	}
    }
    int i = 0;
    while (i < 3) {
    int ligne = lireligne(fd_in, buffer, 4096);
    int nbecrit = write(fd_out, buffer, ligne);
    i++;
    }   
    char *b;
    char pix;
    int sz;
    int wsz;
    do {
    	sz = read(fd_in, b, 1);
    	pix = 255 - b[sz];
    	wsz = write(fd_out, &pix, 1);
    }
    while(sz > 0);
    close(fd_in);
    close(fd_out);
    return 0;
}



int main(int argc, char **argv) {

    int fd_in = open(argv[1],O_RDONLY);
    int fd_out = open(argv[2],O_WRONLY | O_TRUNC);
    int nbread;
    unsigned char *buffer = malloc(4096 * sizeof(unsigned char));  

    if (fd_in < 0) {
        if (errno == ENOENT) {
            perror(argv[1]);
        }
    }
    if (fd_out < 0) {

        if (errno == ENOENT) {
            perror(argv[2]);
        }
    }
    int i = 0;
    while (i < 3) {
    int ligne = lireligne(fd_in, buffer, 4096);
    int nbecrit = write(fd_out, buffer, ligne);
    i++;   
    }
    char *b;
    char pix;
    int sz;
    int wsz;
    do {
        sz = read(fd_in, b, 1);
        pix = 255 - b[sz];
        wsz = write(fd_out, &pix, 1); 
    }
    while(sz > 0);
    close(fd_in);
    close(fd_out);
    return 0;
}

int main(int argc, char **argv) { 
    if(argc != 4){
        printf("Erreur nb args.");
    }
    int fd_in = open(argv[1], O_RDONLY);  
    int fd_out = open(argv[2], O_WRONLY | O_CREAT, 777);
    int nbread;
    unsigned char *buffer = malloc(4096 * sizeof(unsigned char));    // buffer de lecture
    if(fd_in < 0 || fd_out < 0){
        printf("Error.\n");
        perror(argv[1]);
        perror(argv[2]);
        exit(1);
    }
    for(int i=0; i<3; i++){
        int sizeLire = lireligne(fd_in, buffer, 4096);
        int sizeWrite = write(fd_out, buffer, sizeLire);
    }
    int size = 0;
    int ajout = atoi(argv[3]);
    do{
        size = read(fd_in, buffer, 1);
        if(buffer[0] >= 255-ajout){
            buffer[0] = 255;
        }else if (buffer[0] <= 0 + ajout){
            buffer[0] = 0;
        }else {
            buffer[0] = buffer[0]+ajout;
        }
        int sizeWrite = write(fd_out, buffer, 1);
    }while(size>0);
    close(fd_in);
    close(fd_out);
    return 0;


int main(int argc, char **argv) {

    int fd_in = open(argv[1],O_RDONLY);
    int fd_out = open(argv[2],O_WRONLY | O_TRUNC);
    int nbread;
    unsigned char *buffer = malloc(4096 * sizeof(unsigned char));    // buffer de lecture

    if (fd_in < 0) {

    	if (errno == ENOENT) {
    		perror(argv[1]);
    	}
    }
    if (fd_out < 0) {

    	if (errno == ENOENT) {
    		perror(argv[2]);
    	}
    }
    int i = 0;
    while (i < 3) {
    int ligne = lireligne(fd_in, buffer, 4096);
    int nbecrit = write(fd_out, buffer, ligne);
    i++;
    }  
    char *b;
    char pix;
    int sz;
    int wsz;
    do {
    	sz = read(fd_in, b, 1);
    	pix = 255 - b[sz];
    	wsz = write(fd_out, &pix, 1);
    }
    while(sz > 0);
    close(fd_in);
    close(fd_out);
    return 0;
}

int main(int argc, char **argv) {

    int fdin, fdout; 
    char buffer[4096];  
    int nbread;
    fdin = open(argv[1],O_RDONLY);
    fdout = open(argv[2],O_RDONLY | O_TRUNC | O_CREAT);
    if (fdin < 0) {
        perror(argv[1]);
    }
    if (fdout < 0) {
        perror(argv[2]);
    }

    read(fdin,buffer,44);
    int* newTaille = (int *) (buffer + 4);
    *newTaille = ((*newTaille - 36)/2)+36;

    short int* NbrCanaux = (int *) (buffer + 22);
    *NbrCanaux = (*NbrCanaux - 1);

    int* BytePerSec = (int *) (buffer + 28);
    *BytePerSec = *BytePerSec/2;

    short int* BytePerBlock = (int *) (buffer + 32);
    *BytePerBlock = *BytePerBlock/2;

    int* DataSize = (int * ) (buffer + 40);
    *DataSize = *DataSize/2;

    write(fdout,buffer,44);
    int i = 0;
    do {
        int lecture = read(fdin,buffer,4096);
        for (int i = 0 ; i < lecture/2 ; i=i+2) {
        buffer[i] = buffer[2*i];
        buffer[i+1] = buffer[2*i+1];
        }
        write(fdout, buffer, 4096/2);
    }
    while(lecture == 4096);
}